/*******************************************************************************
 * Javalfr
 * Copyright (C) 2021 - auroras.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author _kaos_ (kaos@auroras.fr)        
 *******************************************************************************/
package fr.auroras.javalfr.server;

import org.apache.commons.cli.CommandLine;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import fr.auroras.javalfr.Javalfr;

@SpringBootApplication
public class JavalfrServer extends Javalfr {
	public JavalfrServer(CommandLine cmd) {
		super();
	}

	@Override
	public void run(String[] args) {
		// TODO Auto-generated method stub

	}

	public String getConfigurationFilename() {
		return "ConnectedClient";
	}

	@Override
	public int getLoadersLength() {
		return 1;
	}

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = new SpringApplicationBuilder(JavalfrServer.class).headless(false).run(args);
		ctx.getBean(JavalfrServer.class).run(args);
	}

}
