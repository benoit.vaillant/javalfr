/*******************************************************************************
 * Javalfr
 * Copyright (C) 2021 - auroras.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author _kaos_ (kaos@auroras.fr)        
 *******************************************************************************/
package fr.auroras.javalfr;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class JavalfrApp {
	private static final Logger LOGGER = LogManager.getLogger(JavalfrApp.class);

	@Autowired
	private JavalfrClient javalfrClient;

    @Autowired
    private Environment environment;

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = new SpringApplicationBuilder(JavalfrApp.class).headless(false).run(args);
		ctx.getBean(JavalfrApp.class).run(args);
	}

	public void run(String[] args) {
		JavalfrApp.LOGGER.info("Running JavalfrApp...");
		JavalfrApp.LOGGER.info("Profiles...");
		for (String profileName : environment.getActiveProfiles()) {
			JavalfrApp.LOGGER.info("Currently active profile - " + profileName);
        }
		javalfrClient.run(args);
	}

}
