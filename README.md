# javalfr

version 0.0.1

# About

javalfr (prononcez « j'a va l'faire », surtout si votre chef est dans
votre dos), est un outil de navigation du site linuxfr et que vous
n'avez rien à butiner de mieux pour le moment.

Je ne suis pas certain qu'il arrive même à une version 0.0.2 ceci-dit.

Licence : GPL v3

## Compile, package, install

`mvn clean install -f javalfr-pom.xml`

## Run

Par exemple pour le client Standalone :

`cd javalfr-clients; mvn spring-boot:run -P StandaloneClient; cd -`
