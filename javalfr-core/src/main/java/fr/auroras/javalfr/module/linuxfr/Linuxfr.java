/*******************************************************************************
 * Javalfr
 * Copyright (C) 2021 - auroras.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author _kaos_ (kaos@auroras.fr)        
 *******************************************************************************/
package fr.auroras.javalfr.module.linuxfr;

import fr.auroras.javalfr.module.JavalfrModule;

public class Linuxfr implements JavalfrModule {

	@Override
	public String getName() {
		return "linuxfr";
	}

	@Override
	public boolean isBuiltIn() {
		return true;
	}

}
