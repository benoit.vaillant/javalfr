/*******************************************************************************
 * Javalfr
 * Copyright (C) 2021 - auroras.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author _kaos_ (kaos@auroras.fr)        
 *******************************************************************************/
package fr.auroras.javalfr;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Option.Builder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.auroras.javalfr.lang.Lang;
import fr.auroras.javalfr.setup.JavalfrSetup;
import fr.auroras.javalfr.ui.ModalDialogWindow;
import fr.auroras.javalfr.ui.basic.conf.BasicConfigurationPanel;

/**
 * @author _kaos_ (kaos@auroras.fr) This file is part of Javalfr. Javalfr is
 *         free software: you can redistribute it and/or modify it under the
 *         terms of the GNU General Public
 *         License as published by the Free Software Foundation, either version
 *         3 of the License, or (at your option) any later version. Javalfr is
 *         distributed in the hope that it
 *         will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *         warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 *         the GNU General Public License
 *         for more details. You should have received a copy of the GNU General
 *         Public License along with Javalfr. If not, see
 *         <http://www.gnu.org/licenses/>.
 */

public abstract class Javalfr {
	private static final Logger LOGGER = LogManager.getLogger(Javalfr.class);

	// Just numbering things. Maybe some day this will go into CI
	public static int MAJOR = 0;
	public static int MINOR = 0;
	public static int REVISON = 1;

	// operation modes
	public static int SERVER = 1;
	public static int CLIENT = 2;
	public static int STANDALONE = 3;

	protected boolean headless = false;
	// app setup
	private JavalfrSetup setup;
	// app packs
	private static List<Class<JavalfrPack>> BUILTIN_PACKS = Arrays.asList(new Class[] { Lang.class });
	// a bit of other fields
	private static Lang language;

	// a bit of constants
	public static String DEFAULT_CONFIG_DIR = ".config/javalfr";
	public static int MAIN_UNFIXABLE_ERROR = -1;
	public static int USAGE_EXIT = -2;

	public abstract void run(String[] args);

	public void run2(String[] args) {
		Javalfr.setLang(null); // we don't know the language at this stage
		Javalfr.LOGGER.info(Javalfr.getLang().get("Welcome to Javalfr (version %s)! Have a good day!", Javalfr.getVersion()));

		Options options = Javalfr.makeOptions();

		CommandLineParser parser = new DefaultParser();
		CommandLine cmd = null;
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException pe) {
			Javalfr.usage(options, true);
		}
		// check if help is asked for
		if (cmd.hasOption("help")) {
			Javalfr.usage(options, true);
		}

		try {
			UIManager.setLookAndFeel(getUILnF());
		} catch (UnsupportedLookAndFeelException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			LOGGER.error("Could not load Look and feel, trying headlessly");
			this.setHeadless(true);
		}
		setup = null;

		if (setup == null) {
			Javalfr.LOGGER.error(Javalfr.getLang().get("System not set setup, please rerun or debug this program!"));
			System.exit(Javalfr.MAIN_UNFIXABLE_ERROR);
		}
	}

	private static Options makeOptions() {
		Options options = new Options();
		options.addOption(
				buildOption("t", "translate", Javalfr.language.get("Make a pass on the code and return untranslated strings for a given language"), Javalfr.language.get("lang"))
						.build());
		options.addOption(buildOption("h", "help", Javalfr.language.get("Displays this help message (and exit)")).build());
		options.addOption(buildOption("c", "config", Javalfr.language.get("Use an alternate configuration file"), "config").build());
		options.addOption(buildOption("l", "licenses", Javalfr.language.get("Display the various licenses used in this project")).build());
		return options;
	}

	/**
	 * Wrapper to next buildOption
	 * 
	 * @param shortName
	 * @param longName
	 * @param desc
	 * @return
	 */
	public static Builder buildOption(String shortName, String longName, String desc) {
		return buildOption(shortName, longName, desc, null);
	}

	/**
	 * A basic Option builder function This function does not handle multiple
	 * arguments
	 * 
	 * @param shortName
	 * @param longName
	 * @param desc
	 * @param options
	 * @return
	 */
	public static Builder buildOption(String shortName, String longName, String desc, String arg) {
		Builder builder = Option.builder(shortName);
		if (longName != null)
			builder.longOpt(longName);
		if (desc != null) {
			builder.desc(desc);
		} else {
			builder.desc(Javalfr.language.get("Sorry, this option is not documented yet"));
		}
		if (arg != null) {
			builder.hasArg(true);
			builder.argName(arg);
			builder.valueSeparator(' ');
		}
		return builder;
	}

	/**
	 * Print the program usage function
	 * 
	 * @param exit Do we exit or not after displaying the usage message
	 */
	private static void usage(Options options, boolean exit) {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp(Javalfr.class.getSimpleName(), options);
		if (exit) {
			System.exit(Javalfr.USAGE_EXIT);
		}
	}

	/**
	 * Returns the app configuration file if any specified on the command line.
	 * Otherwise the default one.
	 * 
	 * @param cmd
	 * @return
	 */
	private static File getConfigFile(CommandLine cmd) {
		String homedir = System.getProperty("user.home");
		File mainConfigFile;
		if (cmd == null || !cmd.hasOption("config")) {
			mainConfigFile = new File(homedir, Javalfr.DEFAULT_CONFIG_DIR);
		} else {
			mainConfigFile = new File(cmd.getOptionValue("config"));
		}
		if (!mainConfigFile.exists()) {
			// create this file, it's needed
			try {
				Javalfr.LOGGER.info(mainConfigFile.canWrite());
				mainConfigFile.createNewFile();
			} catch (IOException e) {
				Javalfr.LOGGER.error(Javalfr.getLang().get("Can't create config file. Aborting all operations, sorry!"));
				System.exit(Javalfr.MAIN_UNFIXABLE_ERROR);
			}
		}
		return mainConfigFile;
	}

	protected List<Class<JavalfrPack>> getPacks() {
		return BUILTIN_PACKS;
	}

	private static JavalfrSetup setup(Options options) {
		String uiType = options.getOption("uiType").getValue();
		if (uiType == null) {
			uiType = BasicConfigurationPanel.class.getName();
		}
		ModalDialogWindow config = null;
		try {
			config = (ModalDialogWindow) Class.forName(uiType).getConstructor(new Class[] {}).newInstance();
		} catch (Exception e) {
			Javalfr.LOGGER.error(Javalfr.getLang().get("Could not open config window, sorry :("));
			return null;
		}
		return config.display(language.get("Configuration"));
	}

	public static Lang getLang() {
		if (Javalfr.language == null)
			Javalfr.language = new Lang(null);
		return Javalfr.language;
	}

	public static void setLang(String language) {
		Javalfr.language = new Lang(language);
	}

	public String getUILnF() {
		// default UI look and feel
		return "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
	}

	public static String getVersion() {
		return MAJOR + "." + MINOR + "." + REVISON;
	}

	public abstract int getLoadersLength();

	public boolean isHeadless() {
		return headless;
	}

	public void setHeadless(boolean headless) {
		this.headless = headless;
	}

}
