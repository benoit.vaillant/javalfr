/*******************************************************************************
 * Javalfr
 * Copyright (C) 2021 - auroras.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author _kaos_ (kaos@auroras.fr)        
 *******************************************************************************/
package fr.auroras.javalfr.lang;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.Opcodes;

import fr.auroras.javalfr.JavalfrPack;
import fr.auroras.javalfr.Javalfr;
import fr.auroras.javalfr.lang.introspection.CustomClassVisitor;

/**
 * A language file should contain one line per message you want to translate in
 * the form: initial message=my translation
 * 
 * If the initial message (and thus it's translation) is intended to contain one
 * or more = signs, use the optional formating arguments to insert them in the
 * string.
 *
 */
public class Lang implements JavalfrPack {
	private static final Logger LOGGER = LogManager.getLogger(Lang.class);

	private Map<String, String> localized;

	public static final String LANG_FILTER = Lang.class.getCanonicalName().replace('.', '/');
	// private static final char[] GET_PARAMS = new char[] { 'L', 'L', 'L' };

	public Lang(String lang) {
		localized = loadStrings(lang);
	}

	/**
	 * Loads a translation file and return the associated map
	 * 
	 * @param langFile
	 */
	private static Map<String, String> loadStrings(String lang) {

		if (lang == null)
			lang = System.getProperty("user.language");
		File langFile = null;
		try {
			// FIXME: do a better job at finding langs
			langFile = new File(Lang.class.getClassLoader().getResource("languages/" + lang + ".txt").getFile());
		} catch (NullPointerException npe) {
			Lang.LOGGER.info("No lang for \"" + lang + "\" for now, continuing anyway using default locale...");
		}
		return loadStrings(langFile);
	}

	private static Map<String, String> loadStrings(File langFile) {
		Map<String, String> localized = new HashMap<String, String>();
		if (langFile != null && langFile.exists()) {
			String line;
			boolean bogusLine;
			try {
				BufferedReader reader = new BufferedReader(new FileReader(langFile));
				while ((line = reader.readLine()) != null) {
					bogusLine = true;
					if (line.contains("=")) {
						String[] parts = line.split("=");
						if (parts.length == 2) {
							bogusLine = false;
							if (localized.containsKey(parts[0])) {
								Lang.LOGGER.info(Javalfr.getLang().get("Input string \"%s\" was already, loaded, overwriting", parts[0]));
							}
							localized.put(parts[0], parts[1]);
						}
					}
					if (bogusLine) {
						Lang.LOGGER.info(Javalfr.getLang().get("Input line \"%s\" is not a valid line for a translation file, dropping it", line));
					}
				}
				reader.close();
			} catch (IOException e) {
				Lang.LOGGER.info(Javalfr.getLang().get("Could not load the language file. Continuing anyway..."));
			}
		}
		return localized;
	}

	private static void print(File langFile) {
		print(langFile, null);
	}

	/**
	 * This helper function will print on stream (defaults to console output) what
	 * is not translated in your language file.
	 * 
	 * @param langFile
	 */
	private static void print(File langFile, PrintStream out) {
		if (out == null)
			out = System.out;
		Map<String, String> alreadyTranslated = loadStrings(langFile);
		ClassLoader loader = Javalfr.class.getClassLoader();
		if (loader == null) {
			abortLangParsing("ClassLoader");
		}
		List<File> classes = null;
		File basedir = new File(Javalfr.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		try {
			classes = enumerateClasses(loader, basedir, basedir.getAbsolutePath().length());
		} catch (IOException e) {
			abortLangParsing("classes");
		}
		// now that we've got the classes, we can dig into what's in there

		CustomClassVisitor classVisitor = new CustomClassVisitor(Opcodes.ASM8);
		ClassReader cr = null;
		if (classes != null) {
			for (File f : classes) {
				Lang.LOGGER.info("inspecting class: " + f.getAbsolutePath());
				try {
					cr = new ClassReader(new FileInputStream(f.getAbsolutePath()));
				} catch (IOException e) {
					abortLangParsing("ClassReader/FileInputStream");
				}
				if (cr == null) {
					abortLangParsing("ClassReader");
				}
				cr.accept(classVisitor, 0);

			}
		}

	}

	/**
	 * Go through the code and enumerate classes
	 * 
	 * @param loader
	 * @param pkg
	 * @return
	 * @throws IOException
	 */
	private static List<File> enumerateClasses(ClassLoader loader, File pkg, int prefixSize) throws IOException {
		Stack<File> subPackages = new Stack<File>();
		List<File> classes = new ArrayList<File>();
		subPackages.push(pkg);

		while (!subPackages.isEmpty()) {
			File subPackage = subPackages.pop();

			File[] files = subPackage.listFiles();
			for (File file : files) {
				if (file.isDirectory()) {
					subPackages.push(file);
				}
				if (file.isFile() && file.getName().endsWith(".class")) {
					classes.add(file);
				}
			}
		}
		return classes;
	}

	private static void abortLangParsing(String object) {
		Lang.LOGGER.error(String.format("Could not create the \"%s\" object, can't get untranslated strings", object));
	}

	/**
	 * 
	 * @param string The main string
	 * @param args   The arguments to insert
	 * @return
	 */
	public String get(String string, Object... args) {
		String s = null;
		if (localized != null) {
			s = localized.get(string);
		}
		if (s == null)
			s = string;
		return String.format(s, args);
	}

	@Override
	public boolean isBuiltIn() {
		return true;
	}

	@Override
	public String getName() {
		return "language";
	}

}
