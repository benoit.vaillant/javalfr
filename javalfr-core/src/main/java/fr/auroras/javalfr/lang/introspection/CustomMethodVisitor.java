/*******************************************************************************
 * Javalfr
 * Copyright (C) 2021 - auroras.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author _kaos_ (kaos@auroras.fr)        
 *******************************************************************************/
package fr.auroras.javalfr.lang.introspection;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import fr.auroras.javalfr.lang.Lang;

public class CustomMethodVisitor extends MethodVisitor {
	private int opcode;
	private int access;
	private String owner;
	private String name;
	private String descriptor;
	private boolean isInterface;

	public CustomMethodVisitor(int opcode, int access, String owner, String name, String descriptor,
			boolean isInterface) {
		super(opcode);
		this.opcode = opcode;
		this.access = access;
		this.owner = owner;
		this.name = name;
		this.descriptor = descriptor;
		this.isInterface = isInterface;
	}

	@Override
	public void visitCode() {
		this.visitMethodInsn(this.access, this.owner, this.name, this.descriptor, isInterface);
		this.visitEnd();
	}

	@Override
	public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface) {
		if (Lang.LANG_FILTER.equals(owner) && "get".equals(name)) {
			this.visitVarInsn(Opcodes.ALOAD, 1);
			// super.visitFieldInsn(Opcodes.GETFIELD, owner, name, "Ljava/lang/String;");
			// super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
			System.out.println(" -> " + owner + " " + name + " " + descriptor);
		}
	}

//	@Override
//	public void visitLdcInsn(Object value) {
//		// TODO Auto-generated method stub
//		// super.visitLdcInsn(value);
//		System.out.println("Coucou: " + value.toString());
//	}
}
