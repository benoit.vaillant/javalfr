/*******************************************************************************
 * Javalfr
 * Copyright (C) 2021 - auroras.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author _kaos_ (kaos@auroras.fr)        
 *******************************************************************************/
package fr.auroras.javalfr.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.net.URL;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JWindow;

import fr.auroras.javalfr.Javalfr;

public class SplashScreen extends JWindow {
	public static final int H_GAP = 10;
	public static final int V_GAP = 10;

	private static JProgressBar pbar;
	private static JLabel infoLabel;

	public SplashScreen(URL url, Javalfr javalfr) {
		super();

		setLocationRelativeTo(null);
		setAlwaysOnTop(true);

		JPanel pane = new JPanel(new BorderLayout());
		pane.add(new JLabel(new ImageIcon(url)), BorderLayout.CENTER);

		Box vBox = Box.createVerticalBox();
		vBox.add(Box.createVerticalStrut(V_GAP));

		Box hBox = Box.createHorizontalBox();
		hBox.add(Box.createHorizontalStrut(H_GAP));
		infoLabel = new JLabel(Javalfr.getLang().get("Loading"));
		hBox.add(infoLabel);
		hBox.add(Box.createHorizontalStrut(H_GAP));
		hBox.add(new JLabel(Javalfr.getLang().get("Starting")));
		hBox.add(Box.createHorizontalStrut(H_GAP));

		vBox.add(hBox);

		hBox = Box.createHorizontalBox();
		pbar = new JProgressBar();
		pbar.setMinimum(1);
		pbar.setMaximum(javalfr.getLoadersLength());
		pbar.setStringPainted(true);
		pbar.setForeground(Color.LIGHT_GRAY);

		hBox.add(pbar);

		hBox.add(Box.createHorizontalGlue());
		hBox.add(Box.createHorizontalStrut(H_GAP));

		vBox.add(Box.createVerticalStrut(V_GAP));
		vBox.add(hBox);

		pane.add(vBox, BorderLayout.SOUTH);
		this.setContentPane(pane);

		Dimension dim = pane.getPreferredSize();

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

		setLocation(screenSize.width / 2 - (dim.width / 2), screenSize.height / 2 - (dim.height / 2));

		this.pack();
	}

	public void update(String s) {
		infoLabel.setText(Javalfr.getLang().get(s));
		this.update(getGraphics());
		// this.repaint();
		pbar.setValue(pbar.getValue() + 1);
	}

}
