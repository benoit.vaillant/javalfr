/*******************************************************************************
 * Javalfr
 * Copyright (C) 2021 - auroras.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author _kaos_ (kaos@auroras.fr)        
 *******************************************************************************/
package fr.auroras.javalfr.ui.basic;

import javax.swing.JDialog;
import javax.swing.JFrame;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.auroras.javalfr.Javalfr;
import fr.auroras.javalfr.setup.JavalfrSetup;
import fr.auroras.javalfr.ui.ModalDialogWindow;

public class BasicModalWindow extends JDialog implements ModalDialogWindow {
	private static final long serialVersionUID = -2859157836866664690L;
	private static final Logger LOGGER = LogManager.getLogger(BasicModalWindow.class);

	public ModalDialogWindow MinimalModalWindow(Object displayable, String title) {
		if (!(displayable instanceof JFrame)) {
			// Javalfr javalfr = Javalfr.instance();
			LOGGER.error(Javalfr.getLang().get("Cant create this dialog, exiting, sorry!"));
			System.exit(Javalfr.MAIN_UNFIXABLE_ERROR);
		}

		return this;
	}

	public JavalfrSetup display(String title) {
		// TODO Auto-generated method stub
		return null;
	}

}
