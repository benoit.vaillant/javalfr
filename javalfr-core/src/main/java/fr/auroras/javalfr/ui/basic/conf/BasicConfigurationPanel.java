/*******************************************************************************
 * Javalfr
 * Copyright (C) 2021 - auroras.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author _kaos_ (kaos@auroras.fr)        
 *******************************************************************************/
package fr.auroras.javalfr.ui.basic.conf;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.auroras.javalfr.Javalfr;
import fr.auroras.javalfr.setup.JavalfrSetup;
import fr.auroras.javalfr.ui.ModalDialogWindow;

import net.miginfocom.swing.MigLayout;

public class BasicConfigurationPanel extends JFrame implements ModalDialogWindow {
	private static final long serialVersionUID = -7763232035473604548L;

	public BasicConfigurationPanel() {
		this.setTitle(Javalfr.getLang().get("Javalfr configuration"));
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		this.setContentPane(this.makePanel());
		this.pack();
		this.setVisible(true);
	}

	public JPanel makePanel() {
		JPanel panel = new JPanel(new MigLayout());
		panel.add(new JLabel("test"));

		panel.add(new JLabel("altTest"));
		return panel;
	}

	public JavalfrSetup display(String title) {
		// TODO Auto-generated method stub
		return null;
	}

}
